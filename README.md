# API RestFull NodeJS #

### What is this repository for? ###

* Ce dépôt est une application web Node.Js de type API RestFull, que j'ai développé avec le framework ExpressJS.
* Elle peut être utilisé dans le cadre d'une architecture client-serveur et la combiner avec une ou plusieurs applications de type Client : Angular,... ou les applications mobiles : Android, Apple,...
* La base de données contient les schémas suivants : user, product, brand, category, order, address

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
> Dans le fichier `package.json` sont listé les différents modules dont dépond l'API
* Database configuration
> Pour la base de données j'ai utilisé [MongoDB](https://www.mongodb.com/) (Voir la doc officiel pour l'installation)
* How to run tests
> Utilisez [POSTMAN](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) pour tester les différents web services de l'API.
* Deployment instructions
    * Pour déployer l'application il faut utiliser soit `git` pour cloner le dépôt de ce projet ou télécharger directement son zip.
    * Puis lancer `npm install` dans le dossier du projet pour installer tous les modules.