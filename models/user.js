var mongoose = require('../app/db/mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    lastName : String,
    firstName: String,
    phoneNumber: String,
    email: String,
    address: String,
    login: String,
    password: String
});

var userModel = mongoose.model('user',userSchema);

module.exports = userModel;
