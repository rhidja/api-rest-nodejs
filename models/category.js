var mongoose = require('../app/db/mongoose');

var Schema = mongoose.Schema;

var categorySchema = new Schema({
    name : String,
    description: String
});

var categoryModel = mongoose.model('category',categorySchema);

module.exports = categoryModel;
