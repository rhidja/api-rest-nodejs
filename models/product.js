var mongoose = require('../app/db/mongoose');

var Schema = mongoose.Schema;

var productSchema = new Schema({
    webId: Number,
    name : String,
    brand: {type: Schema.Types.ObjectId, ref: 'brand'},
    category: {type: Schema.Types.ObjectId, ref: 'category'},
    price: Number,
    quantity: Number,
    description: String,
    slider: Boolean,
    characteristics: [{
        name: String,
        value: String
    }],
    reviews: [{
        name: String,
        email: String,
        message: String,
        date: { type: Date, default: Date.now }
    }]
});

var productModel =  mongoose.model('product',productSchema);

module.exports = productModel;
