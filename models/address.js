var mongoose = require('../app/db/mongoose');

var Schema = mongoose.Schema;

var addressSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'user'},
    type: String,
    title: String,
    firstName: String,
    lastName: String,
    email: String,
    phone: String,
    adress: String,
    created: { type: Date, default: Date.now }
});

var addressModel =  mongoose.model('adress',addressSchema);

module.exports = addressModel;
