var mongoose = require('../app/db/mongoose');

var Schema = mongoose.Schema;

var orderSchema = new Schema({
    orderNumber: Number,
    user: {type: Schema.Types.ObjectId, ref: 'user'},
    orderDate: { type: Date, default: Date.now },
    items: [{
        product: {type: Schema.Types.ObjectId, ref: 'product'},
        quantity: Number
    }],
    adress: {type: Schema.Types.ObjectId, ref: 'adress'},
    comments: String
});

var orderModel =  mongoose.model('order',orderSchema);

module.exports = orderModel;
