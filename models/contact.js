var mongoose = require('../app/db/mongoose');

var Schema = mongoose.Schema;

var contactSchema = new Schema({
    name: String,
    emai: String,
    subject: String,
    description: String
});

var contactModel = mongoose.model('contact', contactSchema);

module.exports = contactModel;
