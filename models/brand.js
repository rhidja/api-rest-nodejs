var mongoose = require('../app/db/mongoose');

var Schema = mongoose.Schema;

var brandSchema = new Schema({
    name : String,
    description: String
});

var brandModel = mongoose.model('brand', brandSchema);

module.exports = brandModel;
