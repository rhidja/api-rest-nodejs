var express = require('express');
var jwt     = require('jsonwebtoken'); // used to create, sign, and verify tokens
var nodemailer = require('nodemailer');

var userModel = require('../models/user');
var contactModel = require('../models/contact');

var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    userModel.find(function(err, users){
        res.send(users);
    });
});

/* POST add users. */
router.post('/login', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    if(!req.body.json){
        var user = userModel(req.body);
    }else{
        var user = userModel(JSON.parse(req.body.json));
    }

    userModel.findOne({"email": user.email, "password": user.password}, function(err, user){
        if (err)
            throw err;

        if(!user){
            res.send({ status : false, message : "Email ou mot de passe incorrect."});
        }else {
            // if user is found and password is right
            // create a token
            var token = jwt.sign(user, "superSecret'", {
                expiresIn : 1 //1440 // expires in 24 hours
            });
            res.send({ status : true, message : "Bienvenue!", user: user, token: token});
        }
    });
});

/* POST register user. */
router.post('/register', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    if(!req.body.json){
        var user = userModel(req.body);
    }else{
        var user = userModel(JSON.parse(req.body.json));
    }

    userModel.findOne({$or : [ {"login": user.login}, {"email": user.password}]}, function(err, doc){

        if(!doc){

            user.save(function(err) {
                if (err)
                    throw err;

                // if user is found and password is right
                // create a token
                var token = jwt.sign(user, "superSecret'", {
                    expiresIn : 1 //1440 // expires in 24 hours
                });
                res.send({ status : true, message : "Bienvenue!", user: user, token: token});
            });
        }else {
            res.send({ status : false, message : "Vous êtes déjà inscrit avec cet email."});
        }
    });
});

/* POST add users. */
router.post('/add', function(req, res, next) {

    userModel.findById({login: req.body.login}, function(err, user){
        if(!user){
            var user = userModel(req.body);
            user.save(function(err) {
                if (err)
                    throw err;
                res.send({ status : true, message : "L'usager a été ajouté."});
            });
        }else{
            res.send({status: false, message: "Ce login est déjà pris."});
        }
    });
});

/* POST update users. */
router.post('/update', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    userModel.findById(req.body._id, function(err, user){
        if (!user) {
            res.send({ status : false, message : "L'usager n'existe pas"});
        }else {
            user.firstName = req.body.firstName;
            user.lastName = req.body.lastName;
            user.mobile = req.body.mobile;
            user.email = req.body.email;
            user.adress = req.body.adress;
            user.login = req.body.login;
            user.password = req.body.password;

            user.save(function(err){
                if (err)
                    throw err;

                res.send({ status : true, message : "L'usager a été modifié"});
            });
        }
    });
});

/* POST delete user. */
router.post('/delete', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');
    userModel.findByIdAndRemove(req.body._id,function(err, user){
        if(!user){
            res.send({ status : false, message : "L'usager n'existe pas"});
        }else {
            res.send({ status : true, message : "L'usager a été suprrimé"});
        }
    }).exec();
});

/**
 * --------------------------------------------------------------------------
 * Handle users contacts  ---------------------------------------------------
 * --------------------------------------------------------------------------
 */

/* GET contacts listing. */
router.get('/contacts', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    contactModel.find(function(err, contacts){
        res.send(contacts);
    });
});

/* POST add contact. */
router.post('/contacts', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    /*
    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport('smtps://YOURMAIL@gmail.com:PASSWORD@smtp.gmail.com');

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"Fred Foo ?" <ram34@hotmail.fr>', // sender address
        to: 'rhidja@gmail.com, ram34@hotmail.fr', // list of receivers
        subject: 'Hello ✔', // Subject line
        text: 'Hello world ?', // plaintext body
        html: '<b>Hello world ?</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
    */

    if(!req.body.json){
        var contact = contactModel(req.body);
    }else{
        var contact = contactModel(JSON.parse(req.body.json));
    }

    contact.save(function(err) {
        if (err)
            throw err;
        res.send({ status : true, message : "Votre message à été envoyé."});
    });
});

module.exports = router;
