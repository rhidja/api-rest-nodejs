var express = require('express');
var productModel = require('../models/product');
var router = express.Router();

/* GET produits listing. */
router.get('/', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    let filters = {};

    if ( req.query.brands !== undefined) {
        filters.brand = {$in : req.query.brands.split(",")};
    }

    if ( req.query.categories !== undefined) {
        filters.category = {$in : req.query.categories.split(",")};
    }

    if ( req.query.slider !== undefined) {
        filters.slider = true;
    }

    productModel.find(filters,function(err, products){
        if(err)
            throw err;
        res.send(products);
    }).populate('brand').populate('category');
});

/* GET product by Id. */
router.get('/:_id', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    productModel.findById(req.params._id,function(err, product){
        res.send(product);
    }).populate('brand').populate('category');
});

/* POST add product. */
router.post('/add', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    productModel.findOne({name: req.body.name},function(err, product){
        if(err)
            throw err;

        if(!product){
            var product = productModel(req.body);
            product.save(function(err) {
                if (err)
                    throw err;

                res.send({ status : true, message : "Le produit a été ajouté"});
            });
        }else {
            res.send({ status : false, message : "Ce produit existe déjà"});
        }
    });
});

/* POST update product. */
router.post('/update', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    if(!req.body.json){
        var product = productModel(req.body);
    }else{
        var product = productModel(JSON.parse(req.body.json));
    }

    productModel.findById(product._id, function(err, doc){
        if(!product){
            res.send({ status : false, message : "Le produit n'existe pas"});
        }else {
            doc.webId = product.webId;
            doc.name = product.name;
            doc.brand = product.brand;
            doc.type = product.type;
            doc.price = product.price;
            doc.quantity = product.quantity;
            doc.description = product.description;
            doc.reviews = product.reviews;

            doc.save(function(err){
                if (err)
                    throw err;

                res.send({status : true, message : "Le produit a été modifié"});
            });
        }
    });
});

/* POST delete product. */
router.delete('/delete', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    productModel.findByIdAndRemove(req.body._id,function(err, product){
        if(!product){
            res.send({ status : false, message : "Le produit n'existe pas"});
        }else {
            res.send({ status : true, message : "Le produit a été suprrimé"});
        }
    }).exec();
});

module.exports = router;
