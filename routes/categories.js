var express = require('express');
var categoryModel = require('../models/category');
var router = express.Router();

/* GET categories listing. */
router.get('/', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    categoryModel.find(function(err, categories){
        if(err)
            throw err;

        res.send(categories);
    });
});

/* POST add category. */
router.post('/add', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    categoryModel.findOne({name: req.body.name}, function(err, category){
        if(err)
            throw err;

        if(!category){
            var category = categoryModel(req.body);

            category.save(function(err) {
                if (err)
                    throw err;

                res.send({ status : true, message : "La catégorie a été ajoutée"});
            });
        }else {
            res.send({ status : false, message : "Cette catégorie existe déjà"});
        }
    });
});

/* POST update category. */
router.put('/update', function(req, res, next) {

    categoryModel.findById(req.body._id, function(err, category){
        if(!category){
            res.send({ status : false, message : "La catégorie n'existe pas"});
        }else {
            category.name = req.body.name;
            category.description = req.body.description;

            category.save(function(err){
                if (err)
                    throw err;

                res.send({ status : true, message : "La catégorie a été modifié"});
            });
        }
    });
});

/* DELETE delete category. */
router.delete('/delete', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    categoryModel.findByIdAndRemove(req.body._id,function(err, category){
        if(!category){
            res.send({ status : false, message : "La catégorie n'existe pas"});
        }else {
            res.send({ status : true, message : "La catégorie a été suprrimé"});
        }
    }).exec();
});

module.exports = router;
