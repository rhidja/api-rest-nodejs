var express = require('express');
var brandModel = require('../models/brand');
var router = express.Router();

/* GET brands listing. */
router.get('/', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    brandModel.find(function(err, brands){
        res.send(brands);
    });
});

/* POST add brand. */
router.post('/add', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    brandModel.findOne({name:req.body.name}, function(err, brand){
        if(err)
            throw err;
        if(!brand){
            var brand = brandModel(req.body);
            brand.save(function(err) {
                if (err)
                    throw err;
                res.send({ status : true, message : "Cette marque est ajoutée"});
            });
        }else {
            res.send({ status : false, message : "Cette marque existe déja"});
        }
    });
});

/* POST update brand. */
router.put('/update', function(req, res, next) {
    // Trouver la marque à modifier
    brandModel.findById(req.body._id, function(err, brand){
        if(!brand){
            res.send({ status : false, message : "Cette marque n'existe pas"});
        }else {
            let brand = brandModel(req.body);
            brand.save(function(err){
                if (err)
                    throw err;
                res.send({ status : true, message : "Cette marque a été modifiée!"});
            });
        }
    });
});

/* POST delete brand. */
router.delete('/delete', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-type', 'application/json');

    brandModel.findByIdAndRemove(req.body._id,function(err, brand){
        if (!brand){
            res.send({ status : false, message : "La marque n'existe pas"});
        }else {
            res.send({ status : true, message : "La marque a été suprrimée"});
        }
    }).exec();
});

module.exports = router;
